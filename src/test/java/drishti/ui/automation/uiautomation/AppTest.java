package drishti.ui.automation.uiautomation;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test()

	public void testLogin() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Certification\\ashwani-workspace\\uiautomation\\src\\main\\java\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		String url = "https://ashu.ameyo.com:8443/app/";

		driver.get(url);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String title = driver.getTitle();

		assertEquals(title, "Ameyo");

		driver.findElement(By.xpath("//input[@type='text' and @class='select-dropdown']")).click();

		java.util.List<WebElement> languageList = driver.findElements(By.xpath("//ul//span[contains(text(),'')]"));

		languageList.get(0).click();

		WebElement userId = driver.findElement(By.xpath("//input[@type='text' and contains(@placeholder,'User')]"));

		userId.clear();

		userId.sendKeys("guddu");

		WebElement pass = driver
				.findElement(By.xpath("//input[@type='password' and contains(@placeholder,'Password')]"));

		pass.clear();
		pass.sendKeys("guddu");

		driver.findElement(By.xpath("//span[contains(text(),'Login')]/ancestor::button")).click();

		try {
			if (driver.findElement(By.xpath("//button[@id='automationButton1' and @type='button']")).isDisplayed()) {

				Thread.sleep(5000);

				driver.findElement(By.xpath("//button[@id='automationButton1' and @type='button']")).click();
				Thread.sleep(5000);

			}
		} catch (Exception e) {
			e.toString();

		}

		String campaignUrl = "agentConfiguration";

		System.out.println(driver.getCurrentUrl());
		assertEquals(driver.getCurrentUrl().contains(campaignUrl), true);
		
		driver.close();

	}

}
