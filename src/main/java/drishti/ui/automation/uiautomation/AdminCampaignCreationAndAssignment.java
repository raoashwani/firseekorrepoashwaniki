package drishti.ui.automation.uiautomation;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class AdminCampaignCreationAndAssignment {

	@Test
	public static void main(String[] args) {

		try {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\Certification\\ashwani-workspace\\uiautomation\\src\\main\\java\\chromedriver.exe");

			WebDriver driver = new ChromeDriver();

			String url = "http://io.ameyo.net:8888/app/";

			driver.get(url);

			String title = driver.getTitle();

			assertEquals(title, "Ameyo");

			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			driver.manage().window().maximize();

			driver.findElement(By.xpath("//input[@type='text' and contains(@placeholder,'User')]"))
					.sendKeys("Administrator");
			driver.findElement(By.xpath("//input[@type='password' and contains(@placeholder,'Password')]"))
					.sendKeys("Administrator");

			driver.findElement(By.xpath("//span[contains(text(),'Login')]/ancestor::button")).click();

			// Force Login
			if (driver.findElement(By.xpath("//button[@id='automationButton1' and @type='button']")).isDisplayed()) {

				Thread.sleep(2000);

				driver.findElement(By.xpath("//button[@id='automationButton1' and @type='button']")).click();
			}
			Thread.sleep(15000);

			String login = "system";
			assertEquals(driver.getCurrentUrl().contains(login), true);

			// To click on the Process tab
			driver.findElement(
					By.xpath("//a[contains(@title,'Process') and @id='automationIdAdminCampaignListingButton']"))
					.click();

			// To click on create campaign
			driver.findElement(By.xpath("//span[contains(text(),'Create New Campaign')]")).click();

			// To select the process
			Thread.sleep(5000);
			driver.findElement(By.xpath("//span[contains(@title,'Select Process')]")).click();

			// To select process and enter process name
			WebElement findElement = driver
					.findElement(By.xpath("//input[contains(@type,'search') and @role='textbox']"));

			findElement.sendKeys("fusion");

			findElement.sendKeys(Keys.ENTER);

			// To select campaign type
			driver.findElement(By.xpath("//input[contains(@value,'Select Campaign') and @type='text']")).click();

			// To select the particular campaign type
			driver.findElement(By.xpath("//ul//span[contains(text(),'Outbound')]")).click();

		} catch (Exception e) {
			e.toString();

		}
	}

}
